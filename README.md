<p align="center">
    <img src="https://sr.ht/3O-k.png" width="728" />
</p>

A completely [clean-room](https://en.wikipedia.org/wiki/Clean_room_design) implementation of Minecraft beta 1.7.3 (circa September 2011). No decompiled code has been used in the development of this software. This is an **implementation** - not a clone. TrueCraft is compatible with Minecraft beta 1.7.3 clients and servers.



![](https://sr.ht/87Ov.png)

*Screenshot taken with [Eldpack](http://eldpack.com/)*

We miss the old days of Minecraft, when it was a simple game. It was nearly perfect. Most of what Mojang has added since beta 1.7.3 is fluff, life support for a game that was "done" years ago. This is our attempt to get back to the original spirit of Minecraft, before there were things like the End, or all-in-one redstone devices, or village gift shops. A simple sandbox where you can build and explore and fight with your friends.

## Compiling

**Use a recursive git clone.**

    git clone --recursive https://github.com/andreabenetton/TrueCraft.git

You need to restore Nuget packages. The easiest way is to open the solution in Visual Studio and build from there.
An updated version of some packages must be retrieved via

    git clone --recursive https://github.com/andreabenetton/GeonBit.UI.git
    git clone --recursive https://github.com/andreabenetton/fNbt.git

To compile it and you'll receive binaries in `TrueCraft.Launcher/bin/Debug/`. Run `TrueCraft.Launcher.exe` to run the client and connect to servers and play singleplayer and so on. Run `TrueCraft.Server.exe` to host a server for others to play on.


## Assets

TrueCraft is compatible with Minecraft beta 1.7.3 texture packs. We ship the Pixeludi Pack (by Wojtek Mroczek) by default. You can install the Mojang assets through the TrueCraft launcher if you wish.

## Blah blah blah

TrueCraft is not associated with Mojang or Minecraft in any sort of official capacity.
