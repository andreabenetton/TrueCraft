﻿// MonoGame - Copyright (C) The MonoGame Team
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.

using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using MonoGame.Framework.Utilities;

namespace MonoGame.Utilities.Png
{
    public class PngWriter
    {
        private const int bitsPerSample = 8;
        private Color[] colorData;
        private readonly ColorType colorType;
        private int height;
        private int width;

        public PngWriter()
        {
            colorType = ColorType.RgbWithAlpha;
        }

        public void Write(Texture2D texture2D, Stream outputStream)
        {
            width = texture2D.Width;
            height = texture2D.Height;

            GetColorData(texture2D);

            // write PNG signature
            outputStream.Write(HeaderChunk.PngSignature, 0, HeaderChunk.PngSignature.Length);

            // write header chunk
            var headerChunk = new HeaderChunk();
            headerChunk.Width = (uint) texture2D.Width;
            headerChunk.Height = (uint) texture2D.Height;
            headerChunk.BitDepth = 8;
            headerChunk.ColorType = colorType;
            headerChunk.CompressionMethod = 0;
            headerChunk.FilterMethod = 0;
            headerChunk.InterlaceMethod = 0;

            var headerChunkBytes = headerChunk.Encode();
            outputStream.Write(headerChunkBytes, 0, headerChunkBytes.Length);

            // write data chunks
            var encodedPixelData = EncodePixelData(texture2D);
            var compressedPixelData = new MemoryStream();

            try
            {
                using (var deflateStream = new ZlibStream(new MemoryStream(encodedPixelData), CompressionMode.Compress))
                {
                    deflateStream.CopyTo(compressedPixelData);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("An error occurred during DEFLATE compression.", exception);
            }

            var dataChunk = new DataChunk();
            dataChunk.Data = compressedPixelData.ToArray();
            var dataChunkBytes = dataChunk.Encode();
            outputStream.Write(dataChunkBytes, 0, dataChunkBytes.Length);

            // write end chunk
            var endChunk = new EndChunk();
            var endChunkBytes = endChunk.Encode();
            outputStream.Write(endChunkBytes, 0, endChunkBytes.Length);
        }

        private byte[] EncodePixelData(Texture2D texture2D)
        {
            var filteredScanlines = new List<byte[]>();

            var bytesPerPixel = CalculateBytesPerPixel();
            var previousScanline = new byte[width * bytesPerPixel];

            for (var y = 0; y < height; y++)
            {
                var rawScanline = GetRawScanline(y);

                var filteredScanline = GetOptimalFilteredScanline(rawScanline, previousScanline, bytesPerPixel);

                filteredScanlines.Add(filteredScanline);

                previousScanline = rawScanline;
            }

            var result = new List<byte>();

            foreach (var encodedScanline in filteredScanlines) result.AddRange(encodedScanline);

            return result.ToArray();
        }

        /// <summary>
        ///     Applies all PNG filters to the given scanline and returns the filtered scanline that is deemed
        ///     to be most compressible, using lowest total variation as proxy for compressibility.
        /// </summary>
        /// <param name="rawScanline"></param>
        /// <param name="previousScanline"></param>
        /// <param name="bytesPerPixel"></param>
        /// <returns></returns>
        private byte[] GetOptimalFilteredScanline(byte[] rawScanline, byte[] previousScanline, int bytesPerPixel)
        {
            var candidates = new List<Tuple<byte[], int>>();

            var sub = SubFilter.Encode(rawScanline, bytesPerPixel);
            candidates.Add(new Tuple<byte[], int>(sub, CalculateTotalVariation(sub)));

            var up = UpFilter.Encode(rawScanline, previousScanline);
            candidates.Add(new Tuple<byte[], int>(up, CalculateTotalVariation(up)));

            var average = AverageFilter.Encode(rawScanline, previousScanline, bytesPerPixel);
            candidates.Add(new Tuple<byte[], int>(average, CalculateTotalVariation(average)));

            var paeth = PaethFilter.Encode(rawScanline, previousScanline, bytesPerPixel);
            candidates.Add(new Tuple<byte[], int>(paeth, CalculateTotalVariation(paeth)));

            var lowestTotalVariation = int.MaxValue;
            var lowestTotalVariationIndex = 0;

            for (var i = 0; i < candidates.Count; i++)
                if (candidates[i].Item2 < lowestTotalVariation)
                {
                    lowestTotalVariationIndex = i;
                    lowestTotalVariation = candidates[i].Item2;
                }

            return candidates[lowestTotalVariationIndex].Item1;
        }

        /// <summary>
        ///     Calculates the total variation of given byte array.  Total variation is the sum of the absolute values of
        ///     neighbour differences.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private int CalculateTotalVariation(byte[] input)
        {
            var totalVariation = 0;

            for (var i = 1; i < input.Length; i++) totalVariation += Math.Abs(input[i] - input[i - 1]);

            return totalVariation;
        }

        private byte[] GetRawScanline(int y)
        {
            var rawScanline = new byte[4 * width];

            for (var x = 0; x < width; x++)
            {
                var color = colorData[y * width + x];

                rawScanline[4 * x] = color.R;
                rawScanline[4 * x + 1] = color.G;
                rawScanline[4 * x + 2] = color.B;
                rawScanline[4 * x + 3] = color.A;
            }

            return rawScanline;
        }

        private int CalculateBytesPerPixel()
        {
            switch (colorType)
            {
                case ColorType.Grayscale:
                    return bitsPerSample / 8;

                case ColorType.GrayscaleWithAlpha:
                    return 2 * bitsPerSample / 8;

                case ColorType.Palette:
                    return bitsPerSample / 8;

                case ColorType.Rgb:
                    return 3 * bitsPerSample / 8;

                case ColorType.RgbWithAlpha:
                    return 4 * bitsPerSample / 8;

                default:
                    return -1;
            }
        }

        private void GetColorData(Texture2D texture2D)
        {
            var colorDataLength = texture2D.Width * texture2D.Height;
            colorData = new Color[colorDataLength];

            switch (texture2D.Format)
            {
                case SurfaceFormat.Single:
                    var floatData = new float[colorDataLength];
                    texture2D.GetData(floatData);

                    for (var i = 0; i < colorDataLength; i++)
                    {
                        var brightness = floatData[i];
                        // Export as a greyscale image.
                        colorData[i] = new Color(brightness, brightness, brightness);
                    }

                    break;

                case SurfaceFormat.Color:
                    texture2D.GetData(colorData);
                    break;

                case SurfaceFormat.Alpha8:
                    var alpha8Data = new Alpha8[colorDataLength];
                    texture2D.GetData(alpha8Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) alpha8Data[i]).ToVector4());

                    break;

                case SurfaceFormat.Bgr565:
                    var bgr565Data = new Bgr565[colorDataLength];
                    texture2D.GetData(bgr565Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) bgr565Data[i]).ToVector4());

                    break;

                case SurfaceFormat.Bgra4444:
                    var bgra4444Data = new Bgra4444[colorDataLength];
                    texture2D.GetData(bgra4444Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) bgra4444Data[i]).ToVector4());

                    break;

                case SurfaceFormat.Bgra5551:
                    var bgra5551Data = new Bgra5551[colorDataLength];
                    texture2D.GetData(bgra5551Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) bgra5551Data[i]).ToVector4());
                    break;

                case SurfaceFormat.HalfSingle:
                    var halfSingleData = new HalfSingle[colorDataLength];
                    texture2D.GetData(halfSingleData);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) halfSingleData[i]).ToVector4());

                    break;

                case SurfaceFormat.HalfVector2:
                    var halfVector2Data = new HalfVector2[colorDataLength];
                    texture2D.GetData(halfVector2Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) halfVector2Data[i]).ToVector4());

                    break;

                case SurfaceFormat.HalfVector4:
                    var halfVector4Data = new HalfVector4[colorDataLength];
                    texture2D.GetData(halfVector4Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) halfVector4Data[i]).ToVector4());

                    break;

                case SurfaceFormat.NormalizedByte2:
                    var normalizedByte2Data = new NormalizedByte2[colorDataLength];
                    texture2D.GetData(normalizedByte2Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) normalizedByte2Data[i]).ToVector4());

                    break;

                case SurfaceFormat.NormalizedByte4:
                    var normalizedByte4Data = new NormalizedByte4[colorDataLength];
                    texture2D.GetData(normalizedByte4Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) normalizedByte4Data[i]).ToVector4());

                    break;

                case SurfaceFormat.Rg32:
                    var rg32Data = new Rg32[colorDataLength];
                    texture2D.GetData(rg32Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) rg32Data[i]).ToVector4());

                    break;

                case SurfaceFormat.Rgba64:
                    var rgba64Data = new Rgba64[colorDataLength];
                    texture2D.GetData(rgba64Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) rgba64Data[i]).ToVector4());

                    break;

                case SurfaceFormat.Rgba1010102:
                    var rgba1010102Data = new Rgba1010102[colorDataLength];
                    texture2D.GetData(rgba1010102Data);

                    for (var i = 0; i < colorDataLength; i++)
                        colorData[i] = new Color(((IPackedVector) rgba1010102Data[i]).ToVector4());

                    break;

                default:
                    throw new Exception("Texture surface format not supported");
            }
        }
    }
}