﻿using Microsoft.Extensions.Logging;
using TrueCraft.API.Networking;

namespace TrueCraft.Core.Logging
{
    public static class PacketLogging
    {
        public static void Log<T>(this IPacket packet, ILogger<T> log, bool clientToServer = false)
        {
            if (clientToServer)
                log.Log(LogLevel.Trace, "[CLIENT > SERVER] 0x{0:X2} {1}", packet.ID, packet.GetType().Name);
            else
                log.Log(LogLevel.Trace, "[SERVER > CLIENT] 0x{0:X2} {1}", packet.ID, packet.GetType().Name);
            foreach (var prop in packet.GetType().GetFields())
                log.Log(LogLevel.Trace, "\t{0} ({1}): {2}", prop.Name, prop.FieldType.Name, prop.GetValue(packet));
            log.Log(LogLevel.Trace, ""); // newline
            // TODO: Log packet payload
        }
    }
}