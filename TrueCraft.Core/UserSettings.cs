﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace TrueCraft.Core
{
    public class UserSettings
    {
        public UserSettings()
        {
            AutoLogin = false;
            Username = "";
            Password = "";
            LastIP = "";
            SelectedTexturePack = TexturePack.Default.Name;
            FavoriteServers = Array.Empty<FavoriteServer>();
            IsFullscreen = false;
            InvertedMouse = false;
            WindowResolution = new WindowResolution
            {
                Width = 1280,
                Height = 720
            };
        }

        public static UserSettings Local { get; set; }

        public bool AutoLogin { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string LastIP { get; set; }
        public string SelectedTexturePack { get; set; }
        public FavoriteServer[] FavoriteServers { get; set; }
        public bool IsFullscreen { get; set; }
        public bool InvertedMouse { get; set; }
        public WindowResolution WindowResolution { get; set; }

        public void Load()
        {
            if (File.Exists(Paths.Settings))
                JsonConvert.PopulateObject(File.ReadAllText(Paths.Settings), this);
        }

        public void Save()
        {
            Directory.CreateDirectory(Path.GetDirectoryName(Paths.Settings) ?? throw new InvalidOperationException());
            File.WriteAllText(Paths.Settings, JsonConvert.SerializeObject(this, Formatting.Indented));
        }
    }

    public class FavoriteServer
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class WindowResolution
    {
        private static readonly List<WindowResolution> resolutions = new List<WindowResolution>();

        public static WindowResolution[] Defaults => resolutions.OrderByDescending(x => x.Width).ToArray();

        public int Width { get; set; }
        public int Height { get; set; }


        public static WindowResolution FromString(string str)
        {
            var tmp = str.Split('x');
            return new WindowResolution
            {
                Width = int.Parse(tmp[0].Trim()),
                Height = int.Parse(tmp[1].Trim())
            };
        }

        public static void Populate(int width, int height)
        {
            resolutions.Add(new WindowResolution
            {
                Width = width,
                Height = height
            });
        }
        
             

        public override string ToString()
        {
            return $"{Width} x {Height}";
        }
    }
}