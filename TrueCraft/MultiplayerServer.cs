﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.Extensions.Logging;
using TrueCraft.API;
using TrueCraft.API.Logic;
using TrueCraft.API.Networking;
using TrueCraft.API.Server;
using TrueCraft.API.World;
using TrueCraft.Core.Lighting;
using TrueCraft.Core.Logic;
using TrueCraft.Core.Networking;
using TrueCraft.Core.Networking.Packets;
using TrueCraft.Core.Profiling;
using TrueCraft.Core.World;

namespace TrueCraft
{
    public class MultiplayerServer : IMultiplayerServer, IDisposable
    {
        private static readonly int MillisecondsPerTick = 1000 / 20;
        private readonly ConcurrentBag<Tuple<IWorld, IChunk>> _chunksToSchedule;

        private readonly Timer _environmentWorker;
        private readonly ILogger<MultiplayerServer> _logger;
        private readonly PacketHandler[] _packetHandlers;

        private readonly QueryProtocol _queryProtocol;
        private readonly Stopwatch _time;

        private bool _blockUpdatesEnabled = true;
        internal object ClientLock = new();
        private TcpListener _listener;

        public MultiplayerServer()
        {
            var reader = new PacketReader();
            PacketReader = reader;
            Clients = new List<IRemoteClient>();
            _environmentWorker = new Timer(DoEnvironment);
            _packetHandlers = new PacketHandler[0x100];
            Worlds = new List<IWorld>();
            EntityManagers = new List<IEntityManager>();
            NodeConfiguration nodeConfiguration = new NodeConfiguration();
            _logger = new LoggerService<MultiplayerServer>(nodeConfiguration.Configuration);
            //_logProviders = new List<ILogProvider>();
            Scheduler = new EventScheduler(this);
            var blockRepository = new BlockRepository();
            blockRepository.DiscoverBlockProviders();
            BlockRepository = blockRepository;
            var itemRepository = new ItemRepository();
            itemRepository.DiscoverItemProviders();
            ItemRepository = itemRepository;
            BlockProvider.ItemRepository = ItemRepository;
            BlockProvider.BlockRepository = BlockRepository;
            var craftingRepository = new CraftingRepository();
            craftingRepository.DiscoverRecipes();
            CraftingRepository = craftingRepository;
            PendingBlockUpdates = new Queue<BlockUpdate>();
            EnableClientLogging = false;
            _queryProtocol = new QueryProtocol(this);
            WorldLighters = new List<WorldLighting>();
            _chunksToSchedule = new ConcurrentBag<Tuple<IWorld, IChunk>>();
            _time = new Stopwatch();

            AccessConfiguration = new AccessConfiguration();

            reader.RegisterCorePackets();
            Handlers.PacketHandlers.RegisterHandlers(this);
        }

        public IList<IEntityManager> EntityManagers { get; }
        public IList<WorldLighting> WorldLighters { get; set; }
        private Queue<BlockUpdate> PendingBlockUpdates { get; }

        internal bool ShuttingDown { get; private set; }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public event EventHandler<ChatMessageEventArgs> ChatMessageReceived;
        public event EventHandler<PlayerJoinedQuitEventArgs> PlayerJoined;
        public event EventHandler<PlayerJoinedQuitEventArgs> PlayerQuit;

        public IAccessConfiguration AccessConfiguration { get; internal set; }

        public IPacketReader PacketReader { get; }
        public IList<IRemoteClient> Clients { get; }
        public IList<IWorld> Worlds { get; }
        public IEventScheduler Scheduler { get; }
        public IBlockRepository BlockRepository { get; }
        public IItemRepository ItemRepository { get; }
        public ICraftingRepository CraftingRepository { get; }
        public bool EnableClientLogging { get; set; }
        public IPEndPoint EndPoint { get; private set; }

        public bool BlockUpdatesEnabled
        {
            get => _blockUpdatesEnabled;
            set
            {
                _blockUpdatesEnabled = value;
                if (_blockUpdatesEnabled) ProcessBlockUpdates();
            }
        }

        public void RegisterPacketHandler(byte packetId, PacketHandler handler)
        {
            _packetHandlers[packetId] = handler;
        }

        public void Start(IPEndPoint endPoint)
        {
            Scheduler.DisabledEvents.Clear();
            if (Program.NodeConfiguration.DisabledEvents != null)
                Program.NodeConfiguration.DisabledEvents.ToList().ForEach(
                    ev => Scheduler.DisabledEvents.Add(ev));
            ShuttingDown = false;
            _time.Reset();
            _time.Start();
            _listener = new TcpListener(endPoint);
            _listener.Start();
            EndPoint = (IPEndPoint) _listener.LocalEndpoint;

            var args = new SocketAsyncEventArgs();
            args.Completed += AcceptClient;

            if (!_listener.Server.AcceptAsync(args))
                AcceptClient(this, args);

            Log(LogLevel.Information, "Running TrueCraft server on {0}", EndPoint);
            _environmentWorker.Change(MillisecondsPerTick, Timeout.Infinite);
            if (Program.NodeConfiguration.Query)
                _queryProtocol.Start();
        }

        public void Stop()
        {
            ShuttingDown = true;
            _listener.Stop();
            if (Program.NodeConfiguration.Query)
                _queryProtocol.Stop();
            foreach (var w in Worlds)
                w.Save();
            foreach (var c in Clients)
                DisconnectClient(c);
        }

        public void AddWorld(IWorld world)
        {
            Worlds.Add(world);
            world.BlockRepository = BlockRepository;
            world.ChunkGenerated += HandleChunkGenerated;
            world.ChunkLoaded += HandleChunkLoaded;
            world.BlockChanged += HandleBlockChanged;
            var manager = new EntityManager(this, world);
            EntityManagers.Add(manager);
            var lighter = new WorldLighting(world, BlockRepository);
            WorldLighters.Add(lighter);
            foreach (var chunk in world)
                HandleChunkLoaded(world, new ChunkLoadedEventArgs(chunk));
        }


        public void Log(LogLevel category, string text, params object[] parameters)
        {
            _logger.Log(category, text, parameters);
        }

        public IEntityManager GetEntityManagerForWorld(IWorld world)
        {
            for (var i = 0; i < EntityManagers.Count; i++)
            {
                var manager = EntityManagers[i] as EntityManager;
                if (manager.World == world)
                    return manager;
            }

            return null;
        }

        public void SendMessage(string message, params object[] parameters)
        {
            var compiled = string.Format(message, parameters);
            var parts = compiled.Split('\n');
            foreach (var client in Clients)
            foreach (var part in parts)
                client.SendMessage(part);
            Log(LogLevel.Information, ChatColor.RemoveColors(compiled));
        }

        public void DisconnectClient(IRemoteClient _client)
        {
            var client = (RemoteClient) _client;

            lock (ClientLock)
            {
                Clients.Remove(client);
            }

            if (client.Disconnected)
                return;

            client.Disconnected = true;

            if (client.LoggedIn)
            {
                SendMessage(ChatColor.Yellow + "{0} has left the server.", client.Username);
                GetEntityManagerForWorld(client.World).DespawnEntity(client.Entity);
                GetEntityManagerForWorld(client.World).FlushDespawns();
            }

            client.Save();
            client.Disconnect();
            OnPlayerQuit(new PlayerJoinedQuitEventArgs(client));

            client.Dispose();
        }

        public bool PlayerIsWhitelisted(string client)
        {
            return AccessConfiguration.Whitelist.Contains(client, StringComparer.CurrentCultureIgnoreCase);
        }

        public bool PlayerIsBlacklisted(string client)
        {
            return AccessConfiguration.Blacklist.Contains(client, StringComparer.CurrentCultureIgnoreCase);
        }

        public bool PlayerIsOp(string client)
        {
            return AccessConfiguration.Oplist.Contains(client, StringComparer.CurrentCultureIgnoreCase);
        }

        private void HandleChunkLoaded(object sender, ChunkLoadedEventArgs e)
        {
            if (Program.NodeConfiguration.EnableEventLoading)
                _chunksToSchedule.Add(new Tuple<IWorld, IChunk>(sender as IWorld, e.Chunk));
            if (Program.NodeConfiguration.EnableLighting)
            {
                var lighter = WorldLighters.SingleOrDefault(l => l.World == sender);
                lighter.InitialLighting(e.Chunk, false);
            }
        }

        private void HandleBlockChanged(object sender, BlockChangeEventArgs e)
        {
            // TODO: Propegate lighting changes to client (not possible with beta 1.7.3 protocol)
            if (e.NewBlock.ID != e.OldBlock.ID || e.NewBlock.Metadata != e.OldBlock.Metadata)
            {
                for (int i = 0, ClientsCount = Clients.Count; i < ClientsCount; i++)
                {
                    var client = (RemoteClient) Clients[i];
                    // TODO: Confirm that the client knows of this block
                    if (client.LoggedIn && client.World == sender)
                        client.QueuePacket(new BlockChangePacket(e.Position.X, (sbyte) e.Position.Y, e.Position.Z,
                            (sbyte) e.NewBlock.ID, (sbyte) e.NewBlock.Metadata));
                }

                PendingBlockUpdates.Enqueue(new BlockUpdate {Coordinates = e.Position, World = sender as IWorld});
                ProcessBlockUpdates();
                if (Program.NodeConfiguration.EnableLighting)
                {
                    var lighter = WorldLighters.SingleOrDefault(l => l.World == sender);
                    if (lighter != null)
                    {
                        var posA = e.Position;
                        posA.Y = 0;
                        var posB = e.Position;
                        posB.Y = World.Height;
                        posB.X++;
                        posB.Z++;
                        lighter.EnqueueOperation(new BoundingBox(posA, posB), true);
                        lighter.EnqueueOperation(new BoundingBox(posA, posB), false);
                    }
                }
            }
        }

        private void HandleChunkGenerated(object sender, ChunkLoadedEventArgs e)
        {
            if (Program.NodeConfiguration.EnableLighting)
            {
                var lighter = new WorldLighting(sender as IWorld, BlockRepository);
                lighter.InitialLighting(e.Chunk, false);
            }
            else
            {
                for (var i = 0; i < e.Chunk.SkyLight.Length * 2; i++) e.Chunk.SkyLight[i] = 0xF;
            }

            HandleChunkLoaded(sender, e);
        }

        private void ScheduleUpdatesForChunk(IWorld world, IChunk chunk)
        {
            chunk.UpdateHeightMap();
            var _x = chunk.Coordinates.X * Chunk.Width;
            var _z = chunk.Coordinates.Z * Chunk.Depth;
            Coordinates3D coords, _coords;
            for (byte x = 0; x < Chunk.Width; x++)
            for (byte z = 0; z < Chunk.Depth; z++)
            for (var y = 0; y < chunk.GetHeight(x, z); y++)
            {
                _coords.X = x;
                _coords.Y = y;
                _coords.Z = z;
                var id = chunk.GetBlockID(_coords);
                if (id == 0)
                    continue;
                coords.X = _x + x;
                coords.Y = y;
                coords.Z = _z + z;
                var provider = BlockRepository.GetBlockProvider(id);
                provider.BlockLoadedFromChunk(coords, this, world);
            }
        }

        private void ProcessBlockUpdates()
        {
            if (!BlockUpdatesEnabled)
                return;
            var adjacent = new[]
            {
                Coordinates3D.Up, Coordinates3D.Down,
                Coordinates3D.Left, Coordinates3D.Right,
                Coordinates3D.Forwards, Coordinates3D.Backwards
            };
            while (PendingBlockUpdates.Count != 0)
            {
                var update = PendingBlockUpdates.Dequeue();
                var source = update.World.GetBlockData(update.Coordinates);
                foreach (var offset in adjacent)
                {
                    var descriptor = update.World.GetBlockData(update.Coordinates + offset);
                    var provider = BlockRepository.GetBlockProvider(descriptor.ID);
                    if (provider != null)
                        provider.BlockUpdate(descriptor, source, this, update.World);
                }
            }
        }

        protected internal void OnChatMessageReceived(ChatMessageEventArgs e)
        {
            if (ChatMessageReceived != null)
                ChatMessageReceived(this, e);
        }

        protected internal void OnPlayerJoined(PlayerJoinedQuitEventArgs e)
        {
            if (PlayerJoined != null)
                PlayerJoined(this, e);
        }

        protected internal void OnPlayerQuit(PlayerJoinedQuitEventArgs e)
        {
            if (PlayerQuit != null)
                PlayerQuit(this, e);
        }

        private void AcceptClient(object sender, SocketAsyncEventArgs args)
        {
            try
            {
                var client = new RemoteClient(this, PacketReader, _packetHandlers, args.AcceptSocket);

                lock (ClientLock)
                {
                    Clients.Add(client);
                }
            }
            catch
            {
                // Who cares
            }
            finally
            {
                args.AcceptSocket = null;

                if (!ShuttingDown && !_listener.Server.AcceptAsync(args))
                    AcceptClient(this, args);
            }
        }

        private void DoEnvironment(object discarded)
        {
            if (ShuttingDown)
                return;

            var start = _time.ElapsedMilliseconds;
            var limit = _time.ElapsedMilliseconds + MillisecondsPerTick;
            Profiler.Start("environment");

            Scheduler.Update();

            Profiler.Start("environment.entities");
            foreach (var manager in EntityManagers) manager.Update();
            Profiler.Done();

            if (Program.NodeConfiguration.EnableLighting)
            {
                Profiler.Start("environment.lighting");
                foreach (var lighter in WorldLighters)
                {
                    while (_time.ElapsedMilliseconds < limit && lighter.TryLightNext())
                    {
                        // This space intentionally left blank
                    }

                    if (_time.ElapsedMilliseconds >= limit)
                        Log(LogLevel.Warning, "Lighting queue is backed up");
                }

                Profiler.Done();
            }

            if (Program.NodeConfiguration.EnableEventLoading)
            {
                Profiler.Start("environment.chunks");
                Tuple<IWorld, IChunk> t;
                if (_chunksToSchedule.TryTake(out t))
                    ScheduleUpdatesForChunk(t.Item1, t.Item2);
                Profiler.Done();
            }

            Profiler.Done(MillisecondsPerTick);
            var end = _time.ElapsedMilliseconds;
            var next = MillisecondsPerTick - (end - start);
            if (next < 0)
                next = 0;

            _environmentWorker.Change(next, Timeout.Infinite);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) Stop();
        }

        ~MultiplayerServer()
        {
            Dispose(false);
        }

        private struct BlockUpdate
        {
            public Coordinates3D Coordinates;
            public IWorld World;
        }
    }
}